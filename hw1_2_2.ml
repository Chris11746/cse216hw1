 (*Problem 12*)
 type 'a stc = {sList : int list}
 let start f = 
   let stc = {sList = []} in (f stc);;
 let push num stc f= 
   let newStc = {sList = num::stc.sList } in f newStc;;
 let pop stc f = match stc.sList with
   |[] -> let newStc = {sList = []} in f newStc
   |p::s -> let newStc = {sList = s} in f newStc;;
 let add stc f = match stc.sList with
   |[] -> let newStc = {sList = []} in f newStc
   |h::s::t -> let newStc = {sList = (h+s)::t} in f newStc
   |h::[] -> let newStc = {sList = h::[]} in f newStc;;
 let mult stc f = match stc.sList with
   |[] -> let newStc = {sList = []} in f newStc
   |h::s::t -> let newStc = {sList = (h*s)::t} in f newStc
   |h::[] ->let newStc = {sList = 0::[]} in f newStc;;
 let clone stc f = match stc.sList with
   |[] -> let newStc = {sList = []} in f newStc
   |h::t -> let newStc = {sList = h::h::t} in f newStc;;
 let rec kpop k f stc= match stc.sList with
   |[] -> let newStc = {sList = []} in f newStc
   |h::t -> if k = -1 then let newStc = {sList = t} in kpop h f newStc else if k > 0 then 
   let newStc = {sList = t} in kpop (k-1) f newStc else let newStc = {sList = h::t} in f newStc;; 
 let halt stc =
   stc.sList;;
 start (push 2) (push 3) (push 4) add mult halt;;
 