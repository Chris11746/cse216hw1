(*Chris Riordan
CSE 216
3/2/2020*)
(*Problem 1*)
let rec pow x y n =
  if n>1 then pow (x*y) y (n-1)
  else if n=1 then x
  else 1;;
pow 2 2 3;;
let rec float_pow x y n =
  if n>1 then float_pow (x*.y) y (n-1)
  else if n=1 then x
  else 1.0;; 
float_pow 2.5 2.5 2;;
(*Problem 2*)
let rec compress (cList : 'a list) : 'a list = match cList with
  |f :: (s :: _ as t) -> if f = s 
  then compress t 
  else f::compress t
  |_ -> [];;
compress ["a";"a";"b";"c";"c";"a";"a";"d";"e";"e";"e"];;
(*Problem 3*)
let rec remove_if (newFun : ('a -> bool)) = function
  |n::l -> if newFun n 
  then remove_if newFun l
  else n::remove_if newFun l
  |[] -> [];;
remove_if (fun x -> x mod 2 = 1) [1;2;3;4;5] ;;
(*Problem 4*)
let rec slice i j c (sList : 'a list) : 'a list = match sList with
  |f::s ->
  if (c<i || c>=j) then slice i j (c+1) s
  else  f::(slice i j (c+1) s)
  |[]->[];;
slice 2 6 0 ["a";"b";"c";"d";"e";"f";"g";"h"];;
(*Problem 5*)
let rec equivsFun1 f g = function
  |[] -> []
  |p::s -> if (f g p) then p::equivsFun1 f g s else equivsFun1 f g s;;
let rec equivsFun2 f g = function
  |[] -> []
  |p::s -> if (f g p) then equivsFun2 f g s else p::(equivsFun2 f g s);;
let rec equivs f = function
  |[] -> []
  |p::s -> let newList = equivsFun2 f p s in equivsFun1 f p (p::s)::equivs f newList;;
equivs (=) [1;2;3;4];;
(*Problem 6*)
let isPrime p =
  let rec isNotDivisor div =
    (p mod div <> 0 && isNotDivisor (div+1)) || (pow div div 2) > p in
  p <> 1 && isNotDivisor 2;;
let goldbach num =
  let rec findPrimes p =
    if isPrime p && isPrime (num - p) then (p,num-p)
    else findPrimes (p+1) in findPrimes 2;;
  goldbach 28;;

 (*Problem 7*)
let rec equiv_on f g = function
  |[] -> true
  |h::t -> if(f h) = (g h) then equiv_on f g t
  else false;;
let f i = i*i;;
let g i = 3*i;;
equiv_on f g [3];;
equiv_on f g [1;2;3];;

(*Problem 8*)
let minn x y =
  if x < y then x
  else y;;
let rec pairwiseFilter cmp = function
  |[]->[]
  |f::s::t -> (cmp f s)::(pairwiseFilter (cmp) t)
  |f::[] -> [f];;
pairwiseFilter minn [1;2;3;4;5];;

(*Problem 9*)
let rec polynomial value = function
    |[] -> 0
    |(c,p)::s -> (c*(pow value value p)) + polynomial value s;;
polynomial 2 [3, 3; -2, 1; 5, 0];;
(*Problem 10*)
let rec powerSet = function
    |[] -> [[]]
    |f::s -> List.fold_left (fun xs s -> (f::s)::s::xs) [] (powerSet s);;
powerSet [1;2;3];;
(*Problem 11*)
type bool_expr =
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr;;
  let rec getBool p val_p q val_q = function
  | Lit x -> if x = p then val_p
             else if x = q then val_q
             else failwith "The expression is invalid"
  | Not e -> not(getBool p val_p q val_q e)
  | And(e1, e2) -> getBool p val_p q val_q e1 && getBool p val_p q val_q e2
  | Or(e1, e2) -> getBool p val_p q val_q e1 || getBool p val_p q val_q e2
let truthTable p q expr =
  [(true,  true,  getBool p true  q true  expr);
   (true,  false, getBool p true  q false expr);
   (false, true,  getBool p false q true  expr);
   (false, false, getBool p false q false expr) ];;
   truthTable "a" "b" (And(Lit("a"), Lit("b")));;
  
   (*Problem 12*)
type 'a stc = {sList : int list}
let start f = 
  let stc = {sList = []} in (f stc);;
let push num stc f= 
  let newStc = {sList = num::stc.sList } in f newStc;;
let pop stc f = match stc.sList with
  |[] -> let newStc = {sList = []} in f newStc
  |p::s -> let newStc = {sList = s} in f newStc;;
let add stc f = match stc.sList with
  |[] -> let newStc = {sList = []} in f newStc
  |h::s::t -> let newStc = {sList = (h+s)::t} in f newStc
  |h::[] -> let newStc = {sList = h::[]} in f newStc;;
let mult stc f = match stc.sList with
  |[] -> let newStc = {sList = []} in f newStc
  |h::s::t -> let newStc = {sList = (h*s)::t} in f newStc
  |h::[] ->let newStc = {sList = 0::[]} in f newStc;;
let clone stc f = match stc.sList with
  |[] -> let newStc = {sList = []} in f newStc
  |h::t -> let newStc = {sList = h::h::t} in f newStc;;
let rec kpop k f stc= match stc.sList with
  |[] -> let newStc = {sList = []} in f newStc
  |h::t -> if k = -1 then let newStc = {sList = t} in kpop h f newStc else if k > 0 then 
  let newStc = {sList = t} in kpop (k-1) f newStc else let newStc = {sList = h::t} in f newStc;; 
let halt stc =
  stc.sList;;
start (push 2) (push 3) (push 4) add mult halt;;
