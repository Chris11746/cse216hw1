(*Problem 11*)
type bool_expr =
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr;;
  let rec getBool p val_p q val_q = function
  | Lit x -> if x = p then val_p
             else if x = q then val_q
             else failwith "The expression is invalid"
  | Not e -> not(getBool p val_p q val_q e)
  | And(e1, e2) -> getBool p val_p q val_q e1 && getBool p val_p q val_q e2
  | Or(e1, e2) -> getBool p val_p q val_q e1 || getBool p val_p q val_q e2
let truthTable p q expr =
  [(true,  true,  getBool p true  q true  expr);
   (true,  false, getBool p true  q false expr);
   (false, true,  getBool p false q true  expr);
   (false, false, getBool p false q false expr) ];;
   truthTable "a" "b" (And(Lit("a"), Lit("b")));;